﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

	public bool showInstWindow = true;

	void OnGUI()
	{
		if (showInstWindow) {
			GUI.Box (new Rect (10, Screen.height - 100, 100, 90), "Change Scene");
			if (GUI.Button (new Rect (20, Screen.height - 70, 80, 20), "Next"))
				LoadNextScene ();
			if (GUI.Button (new Rect (20, Screen.height - 40, 80, 20), "Back"))
				LoadPreScene ();
		}
	}

	void LoadPreScene()
	{
		int nextLevel = SceneManager.GetSceneByName(SceneManager.GetActiveScene().name).buildIndex - 1;
		if( nextLevel <= 0)
			nextLevel = SceneManager.sceneCountInBuildSettings - 1;

		SceneManager.LoadScene(nextLevel);
	}

	void LoadNextScene()
	{
		int nextLevel = SceneManager.GetSceneByName(SceneManager.GetActiveScene().name).buildIndex + 1;
		if( nextLevel >= SceneManager.sceneCountInBuildSettings)
			nextLevel = 0;

		SceneManager.LoadScene(nextLevel);

	}
}
