﻿using UnityEngine;
using System.Collections;

public class FaceUpdate : MonoBehaviour
{
	public bool showInstWindow = true;

	public AnimationClip[] animations;

	Animator anim;

	public float delayWeight;

	void Start ()
	{
		anim = GetComponent<Animator> ();
	}

	void OnGUI ()
	{
		if (showInstWindow) {
			foreach (var item in animations) {
				if (GUILayout.Button (item.name)) {
					anim.CrossFade (item.name, 0);
				}
			}
		}
	}

	float current = 0;


	void Update ()
	{

		if (Input.GetMouseButton (0)) {
			current = 1;
		} else {
			current = Mathf.Lerp (current, 0, delayWeight);
		}
		anim.SetLayerWeight (1, current);
	}
}
